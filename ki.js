(function(){
    'use strict';

    function Ki(el, opts){
        this.$el = $(el);
        this.opts = $.extend({}, this.constructor.init, opts);

        this._arrowKeyListeners();
        this._customKeyListeners();
    }

    Ki.init = {
      delegate: 'a',
      direction: 'vertical',
      links: {},
      circular: false
    };

    Ki.prototype.$items = function() {
      return this.$el.find(this.opts.delegate);
    };

    Ki.prototype.arrowKeys = {
      left: 37,
      up: 38,
      right: 39,
      down: 40
    };

    Ki.prototype.customKeys = {
      32: 'SPACE',
      27: 'ESC'
    };

    Ki.prototype.keyName = function(e) {
      return this.customKeys[e.keyCode] || String.fromCharCode(e.keyCode);
    };

    Ki.prototype._arrowKeyListeners = function () {
      var self = this;

      this.$el.on('keydown', self.opts.delegate, function (e) {
        if (!/(37|38|39|40)/.test(e.which)) return;
        e.preventDefault();
        if (self.opts.direction == 'vertical') {
          self.move(e, self.arrowKeys.up, self.arrowKeys.down);
        } else {
          self.move(e, self.arrowKeys.left, self.arrowKeys.right);
        }
      });
    };

    Ki.prototype._customKeyListeners = function () {
      var self = this;

      this.$el.on('keyup', self.opts.delegate, function(e){
        if (/(37|38|39|40)/.test(e.which)) return;
        self.$el.trigger(self.keyName(e) +'.ki');
      });
    };

    Ki.prototype.move = function (e, prev, next) {
      var current_index = this.$items().index(e.target),
          keyCode = e.which;

      if ([prev, next].indexOf(keyCode) > -1) {
        var dir = keyCode == prev ? 'prev' : 'next',
            next_index = this.getIndex(e, dir);
        this.moveFocus(keyCode, next_index, current_index);
      }
      else {
        this.moveToLinks(keyCode, current_index);
      }
    };

    Ki.prototype.moveFocus = function (keyCode, next_index, current_index) {
      if( next_index === current_index ){
        !this.opts.circular ? this.moveToLinks(keyCode, current_index) :
                              this.moveCircular(current_index);

      }
      else {
        this.$items().eq(next_index).focus();
      }
    };

    Ki.prototype.moveToLinks = function (keyCode, index) {
      var links = this.opts.links || {};

      $.each(this.arrowKeys, function(k,v){
        if(keyCode == v && links.hasOwnProperty(k)){//keyCode se mora provjeriti inace bi reagirao na svaki registrirani link
          var next_items = $(links[k].selector).find('a'),
              preserve_index = links[k].preserve_index || false,
              next_index = 0;

          if (preserve_index) { next_index = index < next_items.length ? index : 0; }
          else { next_index = k == 'up' ? next_items.length - 1 : 0; }

          next_items.eq(next_index).focus();
          return false;
        }
      });
    };

    Ki.prototype.moveCircular = function (current_index) {
      var items = this.$items(),
          first = 0,
          last = items.length - 1,
          itemToFocus = current_index === first ? last : first;

      items.eq(itemToFocus).focus();
    };

    Ki.prototype.getIndex = function (e, dir) {
      var index = this.$items().index(e.target);
      //console.log(index);
      if (dir == 'prev' && index > 0)                       index-- //up
      if (dir == 'next' && index < this.$items().length - 1)  index++ //down
      if (!~index)                                          index = 0 //if not indexed

      return index;
    };

    Ki.prototype.destroy = function () {
      this.$el.off('.ki');//unregister listeners
      this.$el.removeData();//remove data
    };

    //Expose as jquery plugin
    var plugin = 'ki';
    var Klass = Ki;

    $.fn[plugin] = function(options){
        var method = typeof options === 'string' && options;
        $(this).each(function(){
            var $this = $(this);
            var instance = $this.data(plugin);
            if(instance){
              method && instance[method]();
              return;
            }
            instance = new Klass(this, options);
            $this.data(plugin, instance);
        });
        return this;
    };

    //Expose class
    $.Ki = Ki;


})();
