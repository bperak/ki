# Keyboard input plugin

## Overview
jQuery plugin for keyboard navigation for WCAG websites. Enables navigation with arrow keys by default and registering custom events on other keys like *SPACE* and/or *ESC*.


## Usage:
### Basic arrow keys navigation with default settings via javascript

Simply bind *ki* to desired navigation/list:
```
#!javascript

  $('.left').ki();
```
### Or more verbose configuration via javascript

```
#!javascript

  $('.left').ki({
    delegate: 'a',
    direction: 'vertical',
    links: {
      right: {
        selector: '.right',
        preserve_index: true
      },
      down: {
        selector: '.bottom',
        preserve_index: false
      }
    },
    circular: true
  }).on('SPACE.ki', function(){
    //ex. open dropdown
  }).on('Q.ki', function(){
   //ex. close dropdown
  });
```

## Options

| Name            |Type           |Default        | Description                                                                                                                         |
| ----------------|:-------------:|:-------------:| -------------                                                                                                                       |
| delegate        |selector       |a              | Navigable descendants of the list/navigation.                                                                                                              |
| direction       |string         |vertical       | Direction of the list - 'vertical' or 'horizontal'.                                                                                                |
| circular        |boolean        |false          | Defines whether navigation through list is circular. Overrides possible navigation to lists defined in links.  |
| links           |object         |{}             | Contains objects named after direction (``left, up, right`` or ``down``) defining links to other lists on the page. Each of the objects must define ``selector`` , while defining ``preserve_index`` property is optional.|

> ``selector`` => css selector of the list we want to link current list with. Definition is **mandatory**.

> ``preserve_index`` => Define whether index from current list should be preserved when jumping to this list. Definition is **optional**. When not defined, default behavior is as it is set to **false**.

## Custom keys definition

Apart from basic navigation throughout the lists, one can define custom key events:

```
#!javascript

$('.dropdown').ki({
//optional configuration
  }).on('ESC',function(){
  //define your own functionality; ex. close currently open dropdown
}).on('A', function(){
  //define some other functionality bind to key A
});
```
Right now, in version 1.0.0, it is possible to bind custom key events on space, escape and all char keys. **Use capital letters when defining custom key events.**


## DEMO example

Try demo example provided in repository under /example folder. Feel free to change configuration in app.js so you get bigger picture.

In order to be able to try demo you need to do the following:

  + ``git clone`` this repository
  + run ``bower install``
  + run ``http-server``

## Install instructions
__To install latest version run:__

``bower install https://git@bitbucket.org:bperak/ki.git#master``

__To install stable release version run:__

``bower install https://git@bitbucket.org:bperak/ki.git``

### Who do I talk to? ###

If you have any questions, feel free to contact us:

* Marko Žabčić <marko@effectiva.hr>
* Borna Perak <borna.perak@gmail.com>