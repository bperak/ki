$(function(){


  'use strict';


  console.log('hello');

  $('.left').ki({
    delegate: 'a',
    direction: 'vertical',
    links: {
      right: {
        selector: '.right',
        preserve_index: true
      },
      down: {
        selector: '.bottom',
        preserve_index: false
      }
    },
    circular: true
  }).on('SPACE.ki', function(){
    console.log('space');
  }).on('A.ki', function(){
    console.log('aaaa');
  });

  $('.right').ki({
    delegate: 'a',
    direction: 'vertical',
    links: {
      left: {
        selector: '.left',
        preserve_index: true
      },
      down: {
        selector: '.bottom'
      },
      right: {
        selector: '.right2',
        preserve_index: true
      }
    }
    //,circular: true
  }).on('ESC.ki', function(){
    console.log('escape right list');
  });

  $('.right2').ki({
    delegate: 'a',
    direction: 'vertical',
    links: {
      left: {
        selector: '.right',
        preserve_index: true
      },
      right: {
        selector: '.right3'
      }
    },
    inEvents: {
      esc: function(){
        console.log('escape right menu');
      }
    },
    circular: true
  });

  $('.right3').ki({
    delegate: 'a',
    direction: 'vertical',
    links: {
      left: {
        selector: '.right2',
        preserve_index: true
      },
      down: {
        selector: '.bottom2'
      },
      right: {
        selector: '.left'
      }
    },
    inEvents: {
      esc: function(){
        console.log('escape right menu');
      }
    }
  });

  $('.bottom').ki({
    direction: 'horizontal',
    links: {
      up: {
        selector: '.right'
      },
      right: {
        selector: '.bottom2'
      }
    }
  });

  $('.bottom2').ki({
    direction: 'horizontal',
    links: {
      up: {
        selector: '.right3'
      },
      left: {
        selector: '.bottom'
      },
      down: {
        selector: '.left'
      }
    }
  });

  var count = $('a',$('.left')).length;
  $('.append_item').click(function(e){
    e.preventDefault();
    var title = 'Item ' + ++count;
    $('.left').append('<li><a href="#">'+title+'</a></li>')
  });
});
